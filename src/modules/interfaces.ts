import { IconType } from "react-icons"
import { WebPartContext } from "@microsoft/sp-webpart-base"
import { SPFI } from "@pnp/sp";

export interface QueueItem {
    Id: number;
    Name: string;
    Condition?: string;
    History?: string;
    Seen: boolean;
    Date?: Date;
}

export type voidFunction = () => void

export type hook = () => [boolean, voidFunction]

export interface QueueState {
    queueItems: QueueItem[];
}

export interface IPropertyControlsTestWebPartProps {
    lists: string | string[];
}

export interface ModalProps {
    hidden: boolean;
    sp: SPFI;
    onDismiss: voidFunction;
    onSuccess: voidFunction;
    dialogContentProps: object;
    modalProps: object;
}

export interface AppointmentProps extends ModalProps {
    appointment: QueueItem;
}

export interface IHospitalProps {
    description: string;
    isDarkTheme: boolean;
    environmentMessage: string;
    hasTeamsContext: boolean;
    userDisplayName: string;
    context: WebPartContext;
}

export interface CardProps {
    number: number;
    icon: IconType;
    text: string;
}