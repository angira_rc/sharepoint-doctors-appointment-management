import { useState } from "react";
import { hook, voidFunction } from "./interfaces";

export const useToggle: hook = (def: boolean = false) => {
    const [value, setValue] = useState<boolean>(def)
    
    const toggle: voidFunction = () => setValue(!value)

    return [value, toggle]
}
