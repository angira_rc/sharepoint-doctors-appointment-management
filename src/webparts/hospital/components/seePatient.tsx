import * as React from "react"
// import { Dialog } from "@fluentui/react";
// import { DialogFooter } from "@fluentui/react";
import { AnimatedDialog } from "@pnp/spfx-controls-react/lib/AnimatedDialog"
import { AppointmentProps, voidFunction } from "../../../modules/interfaces"
import { DialogFooter } from "@fluentui/react"

const SeePatient: React.FC<AppointmentProps> = (props: AppointmentProps) => {
    const {
        hidden,
        onDismiss,
        dialogContentProps,
        modalProps,
        appointment,
        onSuccess,
        sp
    } = props

    const [condition, setCondition] = React.useState(appointment.Condition ?? "")
    const [history, setHistory] = React.useState(appointment.History ?? "")

    const seePatient: voidFunction = () => {
        sp.web.lists.getByTitle("Appointments").items.getById(appointment.Id).update({  
            Condition: condition,
            History: history,
            Seen: true
        }).then(() => {  
            onSuccess()
            alert("Appointment Completed")
            setTimeout(onDismiss, 300)
        }).catch(e => {
            alert("An Error Was Encountered")
            console.log(e)
        })
    }

    return (
        <AnimatedDialog
            hidden={ hidden }
            onDismiss={ onDismiss }
            dialogContentProps={ dialogContentProps }
            dialogAnimationInType='fadeInDown'
            dialogAnimationOutType='fadeOutDown'
            modalProps={ modalProps }>
            <form action="#">
                <label htmlFor="condition" className="block text-sm font-medium text-gray-700 dark:text-gray-300 mb-2">Patient's Condition</label>
                <input type="text" id="condition" onChange={ e => setCondition(e.target.value) } className="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" placeholder="Osteoarthritis" required />
                <label htmlFor="condition" className="block text-sm font-medium text-gray-700 dark:text-gray-300 mb-2">Patient's History</label>
                <textarea rows={ 5 } id="condition" onChange={ e => setHistory(e.target.value) } className="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" placeholder="Presented with..." required />
                
            </form>
            <DialogFooter>
                <button onClick={ seePatient } className="flex items-center justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500">
                    Complete
                </button>
                <button onClick={ onDismiss } className="flex items-center justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-black bg-gray-100 hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-300">
                    Discard
                </button>
            </DialogFooter>
        </AnimatedDialog>
    )
}

export default SeePatient