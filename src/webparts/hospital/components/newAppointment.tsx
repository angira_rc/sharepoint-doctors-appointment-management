import * as React from "react"
// import { Dialog } from "@fluentui/react";
// import { DialogFooter } from "@fluentui/react";
import { AnimatedDialog } from "@pnp/spfx-controls-react/lib/AnimatedDialog"
import { ModalProps, voidFunction } from "../../../modules/interfaces"
import { DialogFooter } from "@fluentui/react"
import { DateConvention, DateTimePicker, TimeConvention } from "@pnp/spfx-controls-react"

const NewAppointment: React.FC<ModalProps> = (props: ModalProps) => {
    const {
        hidden,
        onDismiss,
        dialogContentProps,
        modalProps,
        sp,
        onSuccess
    } = props

    const [name, setName] = React.useState("")
    const [date, setDate] = React.useState<Date>()

    const addAppointment: voidFunction = () => {
        sp.web.lists.getByTitle("Appointments").items.add({  
            Title: name,  
            Date: date,
            Seen: false
        }).then(() => {  
            onSuccess()
            alert("Appointment Added")
            setTimeout(onDismiss, 300)
        }).catch(e => {
            alert("An Error Was Encountered")
            console.log(e)
        })
    }

    return (
        <AnimatedDialog
            hidden={ hidden }
            onDismiss={ onDismiss }
            dialogContentProps={ dialogContentProps }
            dialogAnimationInType='fadeInDown'
            dialogAnimationOutType='fadeOutDown'
            modalProps={ modalProps }>
            <form action="#">
                <label htmlFor="name" className="block text-sm font-medium text-gray-700 dark:text-gray-300 mb-2">Patient Name</label>
                <input type="text" id="name" onChange={ e => setName(e.target.value) } className="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" placeholder="Wilson Kimani" required />
                <label htmlFor="name" className="block text-sm font-medium text-gray-700 dark:text-gray-300 mb-2">Appointment Date</label>
                <DateTimePicker
                    dateConvention={DateConvention.DateTime}
                    timeConvention={TimeConvention.Hours24}
                    value={ date }
                    onChange={ setDate } />
            </form>
            <DialogFooter>
                <button onClick={ addAppointment } className="flex items-center justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500">
                    Add Appointment
                </button>
                <button onClick={ onDismiss } className="flex items-center justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-black bg-gray-100 hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-300">
                    Discard
                </button>
            </DialogFooter>
        </AnimatedDialog>
    )
}

export default NewAppointment