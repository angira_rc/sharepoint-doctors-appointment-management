import * as React from 'react'
import { SPFI } from '@pnp/sp'
import styles from './Hospital.module.scss'
import { LuListTodo } from "react-icons/lu"
import { DialogType } from '@fluentui/react'
import { FaUserDoctor } from 'react-icons/fa6'
import { escape } from '@microsoft/sp-lodash-subset'
import { FaUserCircle, FaPlus } from 'react-icons/fa'
import { IoCheckmarkDoneCircleSharp } from 'react-icons/io5'

import { getSP } from '../../../pnpjsConfig'
import { useToggle } from '../../../modules/hooks'
import { QueueItem, IHospitalProps } from '../../../modules/interfaces'

import Card from './card'
import SeePatient from './seePatient'
import NewAppointment from './newAppointment'

const Hospital: React.FC<IHospitalProps> = props => {
    const {
        // description,
        // isDarkTheme,
        // environmentMessage,
        hasTeamsContext,
        userDisplayName,
        context
    } = props

    const _sp: SPFI = getSP(context)

    const [appointments, setAppointments] = React.useState<QueueItem[]>([])
    const [total, setTotal] = React.useState<number>(0)
    const [pending, setPending] = React.useState<number>(0)
    const [seen, setSeen] = React.useState<number>(0)
    const [selected, setSelected] = React.useState<QueueItem>()

    const [patient, togglePatient] = useToggle()
    const [appointment, toggleAppointment] = useToggle()

    const getAppointments = async (): Promise<void> => {
        const res = await _sp.web.lists.getByTitle("Appointments").items.orderBy('Date', true)()
        console.log(res)
        setAppointments(res.map(itm => ({ 
            Id: itm.Id, 
            Name: itm.Title, 
            Condition: itm.Condition, 
            History: itm.History, 
            Seen: itm.Seen,
            Date: itm.Date
        })))

        setTotal(res.length)
        setPending(res.filter(itm => !itm.Seen).length)
        setSeen(res.filter(itm => itm.Seen).length)
    }

    React.useEffect(() => {
        getAppointments()
    }, [props])

    const handleSelect = (itm: QueueItem): void => {
        setSelected(itm)
        togglePatient()
    }

    return (
        <section className={`${styles.hospital} ${hasTeamsContext ? styles.teams : ''}`}>
            {
                selected &&
                <SeePatient
                    sp={ _sp }
                    hidden={ !patient }
                    onDismiss={ togglePatient }
                    appointment={ selected }
                    dialogContentProps={{
                        type: DialogType.normal,
                        title: 'See Patient',
                        subText: `Encounter with ${selected?.Name}`
                    }}
                    onSuccess={ getAppointments }
                    modalProps={{
                        isDarkOverlay: true
                    }} />
            }
            <NewAppointment
                sp={ _sp }
                hidden={ !appointment }
                onDismiss={ toggleAppointment }
                dialogContentProps={{
                    type: DialogType.normal,
                    title: 'Schedule Appointment',
                    subText: 'Set appointment dates with patient',
                }}
                onSuccess={ getAppointments }
                modalProps={{
                    isDarkOverlay: true
                }} />
            <div className="px-3 flex w-full justify-between items-center">
                <div className="flex w-1/2">
                    <FaUserCircle size="5rem" />
                    <div className="pl-3 items-center">
                        <h2 className="text-3xl mb-1">{escape(userDisplayName)}!</h2>
                        <p>Track and manage your schedule for today</p>
                    </div>
                </div>
                <div className="w-1/2 text-right">
                    <button onClick={ toggleAppointment } className="flex items-center justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500">
                        <FaPlus /> Add New Appointment
                    </button>
                </div>
            </div>
            <div className="flex w-full my-4 justify-between items-center">
                <Card number={total} icon={FaUserDoctor} text="Total Appointments" />
                <Card number={pending} icon={LuListTodo} text="Pending Appointments" />
                <Card number={seen} icon={IoCheckmarkDoneCircleSharp} text="Seen Appointments" />
            </div>
            <div className="flex w-full items-center">
                {
                    appointments.length > 0 ?
                        <table className="w-full">
                            <thead>
                                <tr className="text-left">
                                    <th>Patient Name</th>
                                    <th>Appointment Status</th>
                                    <th>Appointment Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    appointments.map((appointment, i) =>
                                        <tr key={ i } className="text-left">
                                            <td>{ appointment.Name }</td>
                                            <td>{ appointment.Seen ? 'Seen' : 'Pending' }</td>
                                            <td>{ appointment.Date }</td>
                                            <td>
                                                <button onClick={ () => handleSelect(appointment) } className="flex items-center justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500">
                                                    View
                                                </button>
                                            </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                    : <h2 className="text-center">No Appointments Scheduled</h2>
                }
            </div>
        </section>
    )
}

// export default class Hospital extends React.Component<IHospitalProps, {}> {
//     public render(): React.ReactElement<IHospitalProps> {
//         const {
//             description,
//             isDarkTheme,
//             environmentMessage,
//             hasTeamsContext,
//             userDisplayName
//         } = this.props;

//         return (
//             <section className={`${styles.hospital} ${hasTeamsContext ? styles.teams : ''}`}>
//                 <div className={styles.welcome}>
//                     <img alt="" src={isDarkTheme ? require('../assets/welcome-dark.png') : require('../assets/welcome-light.png')} className={styles.welcomeImage} />
//                     <h2>Well done, {escape(userDisplayName)}!</h2>
//                     <div>{environmentMessage}</div>
//                     <div>Web part property value: <strong>{escape(description)}</strong></div>
//                 </div>
//                 <div>
//                     <h3>Welcome to SharePoint Framework!</h3>
//                     <p>
//                         The SharePoint Framework (SPFx) is a extensibility model for Microsoft Viva, Microsoft Teams and SharePoint. It&#39;s the easiest way to extend Microsoft 365 with automatic Single Sign On, automatic hosting and industry standard tooling.
//                     </p>
//                     <h4>Learn more about SPFx development:</h4>
//                     <ul className={styles.links}>
//                         <li><a href="https://aka.ms/spfx" target="_blank" rel="noreferrer">SharePoint Framework Overview</a></li>
//                         <li><a href="https://aka.ms/spfx-yeoman-graph" target="_blank" rel="noreferrer">Use Microsoft Graph in your solution</a></li>
//                         <li><a href="https://aka.ms/spfx-yeoman-teams" target="_blank" rel="noreferrer">Build for Microsoft Teams using SharePoint Framework</a></li>
//                         <li><a href="https://aka.ms/spfx-yeoman-viva" target="_blank" rel="noreferrer">Build for Microsoft Viva Connections using SharePoint Framework</a></li>
//                         <li><a href="https://aka.ms/spfx-yeoman-store" target="_blank" rel="noreferrer">Publish SharePoint Framework applications to the marketplace</a></li>
//                         <li><a href="https://aka.ms/spfx-yeoman-api" target="_blank" rel="noreferrer">SharePoint Framework API reference</a></li>
//                         <li><a href="https://aka.ms/m365pnp" target="_blank" rel="noreferrer">Microsoft 365 Developer Community</a></li>
//                     </ul>
//                 </div>
//             </section>
//         );
//     }
// }


export default Hospital