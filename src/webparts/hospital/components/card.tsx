import * as React from "react"
import { CardProps } from "../../../modules/interfaces"

const Card: React.FC<CardProps> = (props) => {
    return (
        <div className="w-1/3 flex justify-center">
            <div className="mx-3 p-3 border border-gray-300 rounded">
                <div className="border p-3 border-gray-200 rounded">
                    <props.icon size="4rem" />
                </div>
                <h2 className="text-lg">{props.text}</h2>
                <h1 className="text-3xl">{props.number}</h1>
            </div>
        </div>
    )
}

export default Card